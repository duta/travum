﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace Travum.Entity
{
    public class Travel
    {
        private string penyelenggara;
        private string alamat;

        public string Penyelenggara { get => penyelenggara; set => penyelenggara = value; }
        public string Alamat { get => alamat; set => alamat = value; }
    }
}