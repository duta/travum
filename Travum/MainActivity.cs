﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Support.V7.App;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;
using Android.Content.PM;
using Android.Gms.Ads;
using System.IO;
using SearchView = Android.Support.V7.Widget.SearchView;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Travum.Adapter;
using Travum.Data;

namespace Travum
{
    [Activity(Label = "@string/app_name", MainLauncher = true, 
        ConfigurationChanges =(ConfigChanges.Keyboard | ConfigChanges.KeyboardHidden |
        ConfigChanges.Orientation | ConfigChanges.ScreenLayout | ConfigChanges.UiMode | ConfigChanges.ScreenSize |
        ConfigChanges.SmallestScreenSize ), Theme = "@style/MyTheme")]
    [IntentFilter(new[] { Intent.ActionSearch })]
    [MetaData("android.app.searchable", Resource = "@xml/searchable")]
    public class MainActivity : AppCompatActivity
    {
        private static string[] cities = new string[] { "Banda Aceh","Langsa","Lhokseumawe",
            "Meulaboh","Sabang","Subulussalam","Denpasar","Pangkalpinang","Cilegon","Serang","Tangerang Selatan",
            "Tangerang","Bengkulu","Gorontalo","Jakarta","Sungai Penuh","Jambi","Bandung","Bekasi","Bogor",
            "Cimahi","Cirebon","Depok","Sukabumi","Tasikmalaya","Banjar","Magelang","Pekalongan","Purwokerto",
            "Salatiga","Semarang","Surakarta","Tegal","Batu","Blitar","Kediri","Madiun","Malang","Mojokerto",
            "Pasuruan","Probolinggo","Surabaya","Pontianak","Singkawang","Banjarbaru","Banjarmasin","Palangkaraya",
            "Balikpapan","Bontang","Samarinda","Tarakan","Batam","Tanjungpinang","Bandar Lampung","Metro","Ternate",
            "Tidore Kepulauan","Ambon","Tual","Bima","Mataram","Kupang","Sorong","Jayapura","Dumai","Pekanbaru",
            "Makassar","Palopo","Parepare","Palu","Bau-Bau","Kendari","Bitung","Kotamobagu","Manado","Tomohon",
            "Bukittinggi","Padang","Padangpanjang","Pariaman","Payakumbuh","Sawahlunto","Solok","Lubuklinggau",
            "Pagaralam","Palembang","Prabumulih","Binjai","Medan","Padang Sidempuan","Pematangsiantar","Sibolga",
            "Tanjungbalai","Tebingtinggi","Yogyakarta"};

        private ArrayAdapter<string> suggestionAdapter;
        private RecyclerView rv;
        private string connStringSqlite = "";
        private TravelRepo travelRepo;

        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            var toolbar = FindViewById<Toolbar>(Resource.Id.ToolbarMain);

            //Toolbar will now take on default actionbar characteristics
            SetSupportActionBar(toolbar);

            SupportActionBar.Title = GetString(Resource.String.app_name);

            suggestionAdapter = new ArrayAdapter<string>(this, Resource.Layout.SuggestItem, Resource.Id.suggest_item, cities);
           
            // Get the intent, verify the action and get the query
            Intent intent = this.Intent;
            if (Intent.ActionSearch.Equals(intent.Action))
            {
                var query = intent.GetStringExtra(SearchManager.Query);
                //doMySearch(query);
            }

            // copy sqlite dari embedded resource ke folder aplikasi
            connStringSqlite = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), "travelumroh.db");
            if (!File.Exists(connStringSqlite)) {
                using (Stream source = System.Reflection.Assembly.GetExecutingAssembly().GetManifestResourceStream("Travum.Resources.db.travelumroh.db"))
                {
                    using (var destination = System.IO.File.Create(connStringSqlite)) // create or overwrite
                    {
                        source.CopyTo(destination);
                    }
                }
            }            
            connStringSqlite = "Data Source=" + connStringSqlite;

            travelRepo = new TravelRepo(connStringSqlite);

            rv = FindViewById<RecyclerView>(Resource.Id.recyclerView);
            rv.SetAdapter(new TravelAdapter(travelRepo.All()));
            rv.SetLayoutManager(new LinearLayoutManager(this));

            // AdMobs
            //var adId = GetString(Resource.String.test_banner_ad_unit_id); // for test
            var adId = "ca-app-pub-7354315263788743~1271663078"; // for release

            MobileAds.Initialize(ApplicationContext, adId); 
            var adView = FindViewById<AdView>(Resource.Id.adView);
           
            var adRequest = new AdRequest.Builder().Build();
            adView.LoadAd(adRequest);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.menu_main, menu);
            
            IMenuItem searchItem = menu.FindItem(Resource.Id.menu_search);
            var searchView = (SearchView)searchItem.ActionView;
            searchView.QueryHint = "Cari Kota ...";

            var searchAutoComplete = (SearchView.SearchAutoComplete)searchView.FindViewById(Resource.Id.search_src_text);
            searchAutoComplete.Adapter = suggestionAdapter;
            searchAutoComplete.ItemClick += (o, i) => {
                var cityName = suggestionAdapter.GetItem(i.Position);

                searchAutoComplete.Text = cityName;
                Toast.MakeText(this, "Travel di " + cityName, ToastLength.Long).Show();
                rv.SetAdapter(new TravelAdapter(travelRepo.FindByCity(cityName)));
            };

            SearchManager searchManager = (SearchManager)GetSystemService(Context.SearchService);

            // Assumes current activity is the searchable activity
            searchView.SetSearchableInfo(searchManager.GetSearchableInfo(ComponentName));

            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
        {
            switch (item.ItemId) {
                case Resource.Id.menu_about:
                    StartActivity(typeof(AboutActivity));
                    break;
            }

            return base.OnOptionsItemSelected(item);
        }

    }
}