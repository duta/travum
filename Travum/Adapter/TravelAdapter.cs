﻿using System;

using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using System.Collections.Generic;
using Travum.Entity;
using Android.Content;

namespace Travum.Adapter
{
    public class TravelAdapter : RecyclerView.Adapter
    {
        public event EventHandler<TravelAdapterClickEventArgs> ItemClick;
        public event EventHandler<TravelAdapterClickEventArgs> ItemLongClick;
        List<Travel> items;
        //Context _context;
        //RecyclerView _recyclerView;

        public TravelAdapter(List<Travel> data)
        {
            items = data;
            //_context = context;
            //_recyclerView = recyclerView;
        }

        // Create new views (invoked by the layout manager)
        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
        {

            //Setup your layout here
            View itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.TravelItem, parent, false);
            //var id = Resource.Layout.__YOUR_ITEM_HERE;
            //itemView = LayoutInflater.From(parent.Context).
            //       Inflate(id, parent, false);

            var vh = new TravelAdapterViewHolder(itemView, OnClick, OnLongClick);
            return vh;
        }

        // Replace the contents of a view (invoked by the layout manager)
        public override void OnBindViewHolder(RecyclerView.ViewHolder viewHolder, int position)
        {
            var item = items[position];

            // Replace the contents of the view with that element
            var holder = viewHolder as TravelAdapterViewHolder;
            
            holder.TravelName.Text = items[position].Penyelenggara;
            holder.TravelAddress.Text = items[position].Alamat;
        }

        public override int ItemCount => items.Count;

        void OnClick(TravelAdapterClickEventArgs args) => ItemClick?.Invoke(this, args);
        void OnLongClick(TravelAdapterClickEventArgs args) => ItemLongClick?.Invoke(this, args);

    }

    public class TravelAdapterViewHolder : RecyclerView.ViewHolder
    {
        public TextView TravelName { get; set; }
        public TextView TravelAddress { get; set; }

        public TravelAdapterViewHolder(View itemView, Action<TravelAdapterClickEventArgs> clickListener,
                            Action<TravelAdapterClickEventArgs> longClickListener) : base(itemView)
        {
            TravelName = itemView.FindViewById<TextView>(Resource.Id.travelName);
            TravelAddress = itemView.FindViewById<TextView>(Resource.Id.travelAddress);

            itemView.Click += (sender, e) => clickListener(new TravelAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
            itemView.LongClick += (sender, e) => longClickListener(new TravelAdapterClickEventArgs { View = itemView, Position = AdapterPosition });
        }
    }

    public class TravelAdapterClickEventArgs : EventArgs
    {
        public View View { get; set; }
        public int Position { get; set; }
    }
}