﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

using Mono.Data.Sqlite;
using Dapper;

namespace Travum.Data
{
    public class TravelRepo
    {
        private string _connString;
        private SqliteConnection _conn;

        public TravelRepo(string connString) {
            _connString = connString;            
        }

        private void Open() {
            _conn = new SqliteConnection(_connString);
            _conn.Open();
        }

        private void Close()
        {
            if (_conn != null)
            {
                _conn.Close();
            }
        }

        public List<Travum.Entity.Travel> All() {
            List<Travum.Entity.Travel> result = new List<Entity.Travel>();
            StringBuilder sb = new StringBuilder();
            sb.Append("select penyelenggara, alamat from  travel");

            Open();
            result = _conn.Query<Travum.Entity.Travel>(sb.ToString()).ToList();
            Close();

            return result;
        }

        public List<Travum.Entity.Travel> FindByCity(string cityName) {
            List<Travum.Entity.Travel> result = new List<Entity.Travel>();
            StringBuilder sb = new StringBuilder();
            sb.Append("select penyelenggara, alamat from  travel where alamat like @cityNameParam");

            Open();
            result = _conn.Query<Travum.Entity.Travel>(sb.ToString(), new { cityNameParam = "%" + cityName +"%"}).ToList();
            Close();

            return result;
        }

    }
}